/*jshint esversion: 6 */

function userapp() {
	const express = require('express');
	const path = require('path');
	const bodyParser = require('body-parser');
	const serveStatic = require("serve-static");
	global.__BASEDIR = __dirname + '/';

	var util = require(path.join(__BASEDIR, 'util'));

	var app = express();

	app.use(
		serveStatic(path.join(__dirname, "public"), {
				maxAge: "1m"
		})
	);

	// Middlewares
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({
		extended : true
	}));

	app.use(function(req, res, next) {
		let pathname = req.url;
		util.log("Request for [" + pathname + "] received.");

		if(pathname === "/") {
			res.writeHead(200, { 'Content-Type':'text/html; charset=utf-8' });
			res.write('I am live');
			res.end();
			return;
		}

		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
		res.header('Access-Control-Allow-Headers', 'content-type, x-access-token, Set-Cookie');
		next();
	});

	// API
	//app.use('/api/users', require(path.join(__BASEDIR, 'api/users')));
	app.use('/api/auth', require(path.join(__BASEDIR, '/api/auth')));


	return app;
};

module.exports = (/*options*/) => userapp();